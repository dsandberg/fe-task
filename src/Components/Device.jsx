import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  card: {
    width: 300,
    margin: 30
  },
  media: {
    height: 140,
  },
  flexspace: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  warning: {
    color: 'orange'
  },
  ok: {
    color: 'RGBA(85, 135, 98, 1.00)'
  }
});


export default function MediaCard({deviceData}) {
  const classes = useStyles();
  const imgPath = `${process.env.PUBLIC_URL}/img/`;

  return (
    <Card className={classes.card}>
        <CardMedia
          className={classes.media}
          image={deviceData.model ? (imgPath + deviceData.model.replace('-', '') + '.jpg') : ''}
          title="Contemplative Reptile"
        />
        <CardContent>
          <div className={classes.flexspace}>
            <Typography gutterBottom variant="h5" component="h2">{deviceData.title ? deviceData.title : ''}</Typography>
            <Typography gutterBottom color='textSecondary' variant="caption" component="p">{deviceData.title ? deviceData.model : ''}</Typography>
          </div>
          <Typography variant="body2" color="textSecondary" component="p"><span>Status: </span>{deviceData.enabled ? <span className={classes.ok}>enabled</span> : <span className={classes.warning}>disabled</span>}</Typography>
          <Typography variant="body2" color="textSecondary" component="p"><span>Connection: </span>{deviceData.connected ? <span className={classes.ok}>connected</span> : <span className={classes.warning}>disconnected</span>}</Typography>
          <Typography variant="body2" color="textSecondary" component="p"><span>Version: </span>{deviceData.version ? deviceData.version : ''}</Typography>
          <Typography variant="body2" color="textSecondary" component="p"><span>Timezone: </span>{deviceData.timezone ? deviceData.timezone : ''}</Typography>

          {deviceData.storages.length ? 
            deviceData.storages.map( each => (
              <Typography key={each.id} variant="body2" color="textSecondary" component="p"><span>{each.id + ' storage : '} </span>{ each.state === 'ok' ? <span className={classes.ok}>{each.state}</span> : <span className={classes.warning}>{each.state}</span>}</Typography>
              ))
            :
            null
          }
            <br/>
            <Typography variant="body2" color="textSecondary" component="p">Description:</Typography>
            <Typography variant="body2" color="textSecondary" component="p">{deviceData.description ? deviceData.description : ''}</Typography>
        </CardContent>
      <CardActions>
      </CardActions>
    </Card>
  );
}