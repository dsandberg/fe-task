import React, { Component } from 'react';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Cancel from '@material-ui/icons/Cancel';
import Dashboard from '@material-ui/icons/Dashboard';

import Login from './Pages/Login';
import Sites from './Pages/Sites';

import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      sites: [],
      activeSite: null,
      devises: []
    }

    this.url = 'http://localhost:4000';

    this.authenticate = this.authenticate.bind(this);
    this.logout = this.logout.bind(this);
  }

  authenticate(user) {
    this.setState({
      user: user
    });
    this.getSitesForUser(user)
  }

  getSitesForUser(user) {
    fetch(`${this.url}/sites?owner=${user.username}`, {
      method: 'GET',
    }).then(res => res.json())
    .then(response => {
      this.setState({
        sites: response,
      })
      this.selectSite(response[0].id)
    })
    .catch(error => {
      console.warn('error: ', error);
    });
  }
  

  logout() {
    this.setState({
      user: {},
      sites: [],
      activeSite: null
    })
  }

  selectSite(id) {
    fetch(`${this.url}/devices?site_id=${id}`, {
      method: 'GET',
    }).then(res => res.json())
    .then(response => {
      this.setState({
        devices: response,
        activeSite: id
      })
    })
    .catch(error => {
      console.log('error: ', error);
    });
  }

  renderNav() {
    return (
      <List>
        {this.state.sites.map( (each, i) => {
          return (
            <ListItem button key={i} onClick={ () => {this.selectSite(each.id)} }>
              <ListItemIcon style={{minWidth: 'unset', margin: '0 auto'}}>
                <Dashboard />
              </ListItemIcon>
            </ListItem>
          )
        })}
  
        <Divider />
        <ListItem button onClick={this.logout}>
          <ListItemIcon style={{minWidth: 'unset', margin: '0 auto'}}>
            <Cancel color="secondary"/> 
          </ListItemIcon >
        </ListItem>
      </List>
    );
  }
  
  render() {
    const imgPath = `${process.env.PUBLIC_URL}/img/`;

    return (
      <div className="App">
          <Drawer variant="permanent">
            <List>
              <ListItem>
                <img src={imgPath + 'axis_logo.png'} style={{width: '60px'}} alt={'Axis logo'}/>
              </ListItem>
            </List>
            {this.state.user.username ? this.renderNav() : null}
          </Drawer>

          <div>
            {this.state.user.username ? null : <Login authenticate={this.authenticate}/>}
            {this.state.activeSite && this.state.user.username ? <Sites devices={this.state.devices} /> : null}
          </div>
      </div>
    );
  }
}

export default App;
