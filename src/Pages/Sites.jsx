import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Device from '../Components/Device';

const styles = theme => ({
  wrapper: {
    paddingLeft: '88px',
    display: 'flex',
    flexWrap: 'wrap'
  }
});

const Sites = (props) =>  {
  const { classes } = props;

  return (
    <div className={classes.wrapper}>
      {props.devices.map((eachDeviceData, i) => (
        <Device key={i} deviceData={eachDeviceData}/>
      ))}
    </div>
  );
}
  
  export default withStyles(styles, { withTheme: true })(Sites);