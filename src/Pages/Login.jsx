import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  wrapper: {
      maxWidth: '400px',
      margin: '0 auto',
      paddingTop: '20%'
  },
  btn: {
    backgroundColor: 'RGBA(247, 232, 6, 1.00)'
  }
});

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      loginFail: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handelLogin = this.handelLogin.bind(this);
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  handelLogin() {
    if (this.state.username && this.state.password) {
      fetch(`http://localhost:4000/users?username=${this.state.username}&password=${this.state.password}`, {
        method: 'GET',
      }).then(res => res.json())
      .then(response => {
        if (response.length) {
          this.setState({
            loginFail: false
          });
          this.props.authenticate(response[0])
        } else {
          this.setState({
            loginFail: true
          })
        }
      })
      .catch(error => {
        this.setState({
          loginFail: true
        })
      });
    }
  }

    render() {
      const { classes } = this.props;

      return (
        <section className={classes.wrapper}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            label="User"
            name="username"
            autoComplete="username"
            autoFocus
            value={this.state.username}
            onChange={this.handleChange}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            value={this.state.password}
            onChange={this.handleChange}
          />
          <Button variant="contained" color="inherit" className={classes.btn} fullWidth={true} onClick={this.handelLogin}>
            Login
          </Button>
          {this.state.loginFail ? <p style={{color: 'orange'}}>Login Failed</p>: null}
        </section>
      );
    }
  }
  
  export default withStyles(styles, { withTheme: true })(Login);