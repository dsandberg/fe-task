## Front-end Uppgift

Min minimalistiska lösning på uppgiften.

Snabb uppstart med create-react-app och komponenter från @material-ui.

ajax request med fetch api

1. npm install
2. npm run dev, alt (npm run api och npm start)
3. http://localhost:3000/, (rest API http://localhost:4000/)

Kul uppgift! hoppas ni uppskattar min lösning utan react-router-dom :D